import { useState } from 'react';
import clsx from 'clsx';
import { BatteryFull, Category2, Profile2User, Edit, Trash, HambergerMenu, ShieldSearch, ArrowRight, Reserve, Add, MouseSquare, Filter, Refresh, Printer, SearchNormal, Document, UserSquare, Candle, ShoppingCart, Truck, LogoutCurve, ArrowLeft, MenuBoard } from 'iconsax-react';
import Menu from './components/Menu';
import Layout from './components/Layout';

function App() 
{
  const [isMenuOpen, setIsMenuOpen] = useState(false);

  return (
    <Layout>
      <div className='flex border-b-2 flex-col xl:flex-row'>
          <div className='pb-4'>
            <div className='text-xl font-bold'>
              Customer
            </div>
            <div className='text-[#98949E]'>
              You can manage and organize your customer and other things here
            </div>
          </div>
          <div className='flex items-end justify-end flex-1 text-[#98949E]'>
            <button className='w-[197.33px] py-3 text-[#5D5FEF] border-b-2 border-b-[#5D5FEF] text-center font-bold'>Customer</button>
            <button className='w-[197.33px] py-3 text-center font-semibold'>Promo</button>
            <button className='w-[197.33px] py-3 text-center font-semibold'>Voucher</button>
          </div>
        </div>
        <div className='mt-4 flex gap-4 flex-1 flex-col xl:flex-row'>
          <div className='flex-1 flex flex-col'>
            <div className='bg-[#5D5FEF] p-3 text-white rounded-lg'>
              <div className='text-lg font-semibold'>Customer</div>
              <div className='xl:max-w-[50%]'>
                On this menu you will be able to create, edit, and also delete the customer. Also you can manage it easily.
              </div>
              <div className='flex gap-4 mt-6 flex-wrap'>
                <button className='px-4 py-2 bg-[#FFFFFF33] rounded-lg flex items-center gap-3'>
                  <Add size={16} />
                  <span>
                    Add new customer
                  </span>
                </button>
                <div className='flex gap-6 items-center rounded-lg bg-white flex-1'>
                  <SearchNormal size={16} color='#98949E' className='ml-6' />
                  <input className='flex-1 text-black outline-o border-0 w-full' type="text" placeholder="Search Customer" />
                  <div className='p-1'>
                    <button className='text-[#5D5FEF] px-4 py-2 rounded-lg flex items-center text-white bg-[#5D5FEF]'>Search</button>
                  </div>
                </div>
                <button className='px-4 py-2 bg-[#FFFFFF33] rounded-lg flex items-center gap-3'>
                  <Filter size={16} />
                  <span>
                    Filter
                  </span>
                </button>
                <button className='px-4 py-2 bg-[#FFFFFF33] rounded-lg flex items-center gap-3'>
                  <Refresh size={16} />
                  <span>
                    Refresh
                  </span>
                </button>
                <button className='px-4 py-2 bg-[#FFFFFF33] rounded-lg flex items-center gap-3'>
                  <Printer size={16} />
                </button>
              </div>
            </div>
            <div className='mt-6 flex-1 flex flex-col justify-between relative overflow-x-scroll'>
              <table className='w-full'>
                <thead>
                  <tr>
                    <th>Customer Name</th>
                    <th>Level</th>
                    <th>Favorite Menu</th>
                    <th>Total Transaction</th>
                    <th>Action</th>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <td>Odis Rhinehart</td>
                    <td>
                      <span className='bg-[#FEFBF6] px-6 py-2 rounded-lg'>
                        <span style={{background: 'linear-gradient(#EEA849, #F46B45)', WebkitBackgroundClip: 'text', WebkitTextFillColor: 'transparent'}}>Warga</span>
                      </span>
                    </td>
                    <td>
                      Chicken & Ribs Combo
                    </td>
                    <td>
                      IDR 194,700
                    </td>
                    <td className='flex gap-1'>
                      <button className='bg-[#FAFAFA] flex gap-2 items-center px-2 py-1 rounded-lg'>
                        <ShieldSearch size={12} />
                        <span>
                          Detail
                        </span>
                      </button>
                      <button className='bg-[#FAFAFA] px-2 py-1 rounded-lg'>
                        <Edit size={12} />
                      </button>
                      <button className='bg-[#FEF5F6] text-[#F02D3A] px-2 py-1 rounded-lg'>
                        <Trash size={12} />
                      </button>
                    </td>
                  </tr>
                  <tr>
                    <td>Odis Rhinehart</td>
                    <td>
                      <span className='bg-[#FEFBF6] px-6 py-2 rounded-lg'>
                        <span style={{background: 'linear-gradient(#EEA849, #F46B45)', WebkitBackgroundClip: 'text', WebkitTextFillColor: 'transparent'}}>Warga</span>
                      </span>
                    </td>
                    <td>
                      Chicken & Ribs Combo
                    </td>
                    <td>
                      IDR 194,700
                    </td>
                    <td className='flex gap-1'>
                      <button className='bg-[#FAFAFA] flex gap-2 items-center px-2 py-1 rounded-lg'>
                        <ShieldSearch size={12} />
                        <span>
                          Detail
                        </span>
                      </button>
                      <button className='bg-[#FAFAFA] px-2 py-1 rounded-lg'>
                        <Edit size={12} />
                      </button>
                      <button className='bg-[#FEF5F6] text-[#F02D3A] px-2 py-1 rounded-lg'>
                        <Trash size={12} />
                      </button>
                    </td>
                  </tr>
                  <tr>
                    <td>Odis Rhinehart</td>
                    <td>
                      <span className='bg-[#FEFBF6] px-6 py-2 rounded-lg'>
                        <span style={{background: 'linear-gradient(#EEA849, #F46B45)', WebkitBackgroundClip: 'text', WebkitTextFillColor: 'transparent'}}>Warga</span>
                      </span>
                    </td>
                    <td>
                      Chicken & Ribs Combo
                    </td>
                    <td>
                      IDR 194,700
                    </td>
                    <td className='flex gap-1'>
                      <button className='bg-[#FAFAFA] flex gap-2 items-center px-2 py-1 rounded-lg'>
                        <ShieldSearch size={12} />
                        <span>
                          Detail
                        </span>
                      </button>
                      <button className='bg-[#FAFAFA] px-2 py-1 rounded-lg'>
                        <Edit size={12} />
                      </button>
                      <button className='bg-[#FEF5F6] text-[#F02D3A] px-2 py-1 rounded-lg'>
                        <Trash size={12} />
                      </button>
                    </td>
                  </tr>
                  <tr>
                    <td>Odis Rhinehart</td>
                    <td>
                      <span className='bg-[#FEFBF6] px-6 py-2 rounded-lg'>
                        <span style={{background: 'linear-gradient(#EEA849, #F46B45)', WebkitBackgroundClip: 'text', WebkitTextFillColor: 'transparent'}}>Warga</span>
                      </span>
                    </td>
                    <td>
                      Chicken & Ribs Combo
                    </td>
                    <td>
                      IDR 194,700
                    </td>
                    <td className='flex gap-1'>
                      <button className='bg-[#FAFAFA] flex gap-2 items-center px-2 py-1 rounded-lg'>
                        <ShieldSearch size={12} />
                        <span>
                          Detail
                        </span>
                      </button>
                      <button className='bg-[#FAFAFA] px-2 py-1 rounded-lg'>
                        <Edit size={12} />
                      </button>
                      <button className='bg-[#FEF5F6] text-[#F02D3A] px-2 py-1 rounded-lg'>
                        <Trash size={12} />
                      </button>
                    </td>
                  </tr>
                </tbody>
              </table>
              <div className='w-full bg-[#FAFAFA] p-3 rounded-lg flex gap-2 justify-between sticky bottom-0 right-0 left-0'>
                <div className='text-[#98949E]'>
                  Showing 10 data customers
                </div>
                <div className='flex text-[#6D6D6D]'>
                  <button className='px-4 py-1 bg-white rounded-lg shadow-xl text-black'>1</button>
                  <button className='px-4'>2</button>
                  <button className='px-4'>3</button>
                  <button className='px-4'>...</button>
                  <button className='px-4'>38</button>
                  <button className='px-4 flex gap-3'>
                    <span>
                      Next
                    </span>
                      <ArrowRight />
                  </button>
                </div>
              </div>
            </div>
          </div>
          <div className='xl:max-w-[227px] w-full flex flex-col'>
            <div className='h-[265px] bg-[#5D5FEF] rounded-lg p-4 text-white flex justify-between flex-col' style={{backgroundSize: 'auto 100%', backgroundPosition: 'right 0% bottom 0%', backgroundRepeat: 'no-repeat', backgroundImage: 'url("http://localhost:3000/circles.png")'}}>
              <div className='text-lg'>See analytics of the customer clearly</div>
              <div>
                <button className='px-4 py-2 bg-[#FFFFFF33] rounded-lg'>
                  See Analytics
                </button>
              </div>
            </div>
            <div className='flex-1 bg-[#FAFAFA] mt-4 rounded-lg p-4' style={{backgroundSize: '100% auto', backgroundPosition: 'bottom 0% right 0%', backgroundRepeat: 'no-repeat', backgroundImage: 'url("http://localhost:3000/graph.png")'}}>
              <div className='text-lg text-xl font-semibold'>
                Top Menu
                <br />
                <span className='text-[#F17300] font-bold'>
                  This Week
                </span>
              </div>
              <div className='text-[#98949E] mt-3'>
                10 - 12 Agustus 2023
              </div>
              <div className='flex flex-col gap-2'>
                <div className='bg-white mt-3 relative rounded-lg shadow-xl p-[10px] font-bold text-[1.1rem] pr-'>
                  Nasi Goreng Jamur Special Resto Pak Min
                  <div className='px-2 top-[-8px] right-0 rotate-[8deg] bg-[#F17300] text-white absolute' style={{boxShadow: '3px 3px 0px 0px rgba(0,0,0,0.75)'}}>
                    1
                  </div>
                </div>
                <div className='text-[#98949E] p-[10px]'>
                  2. Tongseng Sapi Gurih
                </div>
                <div className='text-[#98949E] p-[10px]'>
                  3. Nasi Gudeg Telur Ceker
                </div>
                <div className='text-[#98949E] p-[10px]'>
                  4. Nasi Ayam Serundeng
                </div>
                <div className='text-[#98949E] p-[10px]'>
                  5. Nasi Goreng Seafood
                </div>
              </div>
            </div>
          </div>
        </div>
    </Layout>
  );
}

export default App;

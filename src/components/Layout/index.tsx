import Menu from "../Menu";
import { useState } from 'react';
import clsx from 'clsx';
import { BatteryFull, Category2, Profile2User, Edit, Trash, HambergerMenu, ShieldSearch, ArrowRight, Reserve, Add, MouseSquare, Filter, Refresh, Printer, SearchNormal, Document, UserSquare, Candle, ShoppingCart, Truck, LogoutCurve, ArrowLeft, MenuBoard } from 'iconsax-react';

interface props {
  children?: any
}

export default function Layout({children}: props)
{
  const [isMenuOpen, setIsMenuOpen] = useState(false);

  return (
    <div className='flex h-screen'>
      <Menu setIsMenuOpen={setIsMenuOpen} className={isMenuOpen ? '' : 'hidden xl:flex'} />
      <div className='flex-1 p-6 flex flex-col overflow-y-scroll'>
        <div className='xl:hidden mb-6'>
          <button onClick={() => setIsMenuOpen(true)}>
            <HambergerMenu size={32} />
          </button>
        </div>
        {children}
      </div>
    </div>
  )
}
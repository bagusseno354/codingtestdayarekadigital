import { useState } from 'react';
import clsx from 'clsx';
import { BatteryFull, Category2, Profile2User, Edit, Trash, HambergerMenu, ShieldSearch, ArrowRight, Reserve, Add, MouseSquare, Filter, Refresh, Printer, SearchNormal, Document, UserSquare, Candle, ShoppingCart, Truck, LogoutCurve, ArrowLeft, MenuBoard } from 'iconsax-react';

interface props {
    className?: string,
    setIsMenuOpen: Function
}

export default function Menu({className, setIsMenuOpen}: props)
{
    return (
        <div className={clsx('h-full flex justify-between flex-col border-r-2 fixed xl:relative bg-white z-10 overflow-y-scroll', className)}>
        <div>
          <div className='p-6'>
            <img src='http://localhost:3000/logo.png' alt="" className='h-[35px]' />
            <button className='absolute right-4 top-4 xl:hidden' onClick={() => setIsMenuOpen(false)}>
              <ArrowLeft />
            </button>
          </div>
          <div className='text-[#98949E]'>
            <div className='px-6 py-2 text-sm'>Menu</div>
            <div className='flex gap-3 flex-col'>
              <button className='px-6 py-2 flex items-center justify-between'>
                <div className='flex gap-3 items-center'>
                  <Category2 size={14} />
                  <span>
                    Dashboard
                  </span>
                </div>                
                <span className='rounded-full px-2 text-sm text-white' style={{backgroundImage: 'linear-gradient(#EEA849, #F46B45)'}}>
                  4
                </span>
              </button>
              <button className='px-6 py-2 flex gap-3 items-center'>
                <BatteryFull size={14} />
                <span>
                  Stock
                </span>
              </button>
              <button className='px-6 py-2 flex gap-3 items-center text-[#5D5FEF]'>
                <Profile2User enableBackground='#5D5FEF' size={14} />
                <span>
                  Customer
                </span>
              </button>
              <button className='px-6 py-2 flex gap-3 items-center'>
                <Reserve size={14} />
                <span>
                  Restaurant
                </span>
              </button>             
              <button className='px-6 py-2 flex gap-3 items-center'>
                <MouseSquare size={14} />
                <span>
                  Design
                </span>
              </button>
              <button className='px-6 py-2 flex gap-3 items-center'>
                <Document size={14} />
                <span>
                  Report
                </span>
              </button>
              <button className='px-6 py-2 flex gap-3 items-center'>
                <UserSquare size={14} />
                <span>
                  Role & Admin
                </span>
              </button>
              <button className='px-6 py-2 flex gap-3 items-center'>
                <Candle size={14} />
                <span>
                  Settings
                </span>
              </button>
            </div>
          </div>
          <div className='mt-6 text-[#98949E]'>
            <div className='px-6 py-2 text-sm'>Integration</div>
            <div className='flex gap-3 flex-col'>
              <button className='px-6 py-2 flex gap-3 items-center'>
                <ShoppingCart size={14} />
                <span>
                  Stock
                </span>
              </button>              
              <button className='px-6 py-2 flex gap-3 items-center'>
                <Truck size={14} />
                <span>
                  Supply
                </span>
              </button>
            </div>
          </div>
        </div>
        <div className='p-6'>
          <div className='flex gap-3 items-center'>
            <div>
              <img className='rounded-lg-full' width={'36px'} src='http://localhost:3000/pp.png' alt="" />
            </div>
            <div>
              <div>Savannah N</div>
              <div className='text-sm text-[#98949E]'>Food Quality Manager</div>
            </div>
          </div>
          <div className='mt-6'>
            <button className='bg-[#FEF5F6] text-[#8F0A13] flex items-center justify-center gap-2 w-full p-2'>
              <LogoutCurve size={16} />
              <span>
                Log out
              </span>
            </button>
          </div>
        </div>
      </div>
    )
}